﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Tests
{
    public enum RuleType
    {
        Allow,
        Deny
    }

    class Rule
    {
        public IPNetwork src, dst;
        public int port;
        public RuleType policy;
        public Rule(IPNetwork src, IPNetwork dst, int port, RuleType policy)
        {
            this.src = src;
            this.dst = dst;
            this.port = port;
            this.policy = policy;
        }

        public string Dump()
        {
            string policyStr = policy == RuleType.Allow ? "allow" : "deny";
            return src.ToString().Trim() + "\t" + dst.ToString().Trim() + "\t" + port + "\t" + policyStr;
        }
    }

    class Utils
    {
        public static IPAddressCollection GetIPs(string range)
        {
            IPNetwork iprange = IPNetwork.Parse(range);
            return IPNetwork.ListIPAddress(iprange);
        }

        public static void WriteTable(List<Rule> rules, string fileName)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))
            {
                foreach(Rule rule in rules)
                {
                    file.WriteLine(rule.Dump());
                }
            }
        }

        public static IPNetwork GetRandomIPRange()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            string iprange = rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "/" + Utils.GetRandomNetmask();
            return IPNetwork.Parse(iprange);
        }

        public static int GetRandomNetmask()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            return rnd.Next(0, 33);
        }

        public static IPNetwork GetRandomIPRangeWithCidrNetmask(int netmask)
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            string iprange = rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "/" + netmask;
            return IPNetwork.Parse(iprange);
        }

        public static IPNetwork GetRandomIPRangeShort()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            string iprange = rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "." + rnd.Next(0, 256) + "/" + rnd.Next(24, 33);
            return IPNetwork.Parse(iprange);
        }

        public static int GetRandomPort()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            return rnd.Next(1, ushort.MaxValue);
        }

        public static RuleType GetRandomRuleType()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            int num = rnd.Next(0, 2);
            if (num == 1)
            {
                return RuleType.Allow;
            }
            return RuleType.Deny;
        }

        public static Rule GetRandomRule()
        {
            return new Rule(Utils.GetRandomIPRange(), Utils.GetRandomIPRange(), Utils.GetRandomPort(), Utils.GetRandomRuleType());
        }

        public static Rule GetRandomRuleShortRange()
        {
            return new Rule(Utils.GetRandomIPRangeShort(), Utils.GetRandomIPRangeShort(), Utils.GetRandomPort(), Utils.GetRandomRuleType());
        }
    }
}
