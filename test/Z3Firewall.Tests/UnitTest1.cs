﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void EmptyFiles()
        {
            List<Rule> rules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            string fileName = "Empty_1.txt";
            Utils.WriteTable(rules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "Empty_2.txt";
            Utils.WriteTable(rules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.UNSAT);
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }


        [TestMethod]
        public void FirstIP()
        {
            const int numOfRules = 100;
            List<Rule> rules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<Rule> firstRules = new List<Rule>();
            List<string> files = new List<string>();
            IPNetwork allRange = Utils.GetRandomIPRangeWithCidrNetmask(0);
            int port = Utils.GetRandomPort();

            Rule allRule = new Rule(allRange, allRange, port, RuleType.Allow);
            rules.Add(allRule);
            firstRules.Add(allRule);
            for (int i = 0; i < numOfRules; ++i)
            {
                Rule r = Utils.GetRandomRule();
                r.port = port;
                firstRules.Add(r);
            }

            string fileName = "FirstIP_1_port_" + port + ".txt";
            Utils.WriteTable(rules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "FirstIP_2_port" + port + ".txt";
            Utils.WriteTable(firstRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                if (order == Z3Firewall.Solver.OrderType.FirstMatch)
                {
                    Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.UNSAT);
                } else
                {
                    Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.SAT);
                }
                
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }


        [TestMethod]
        public void DifferentPort()
        {
            const int numOfRules = 100;
            List<Rule> rules = new List<Rule>();
            List<Rule> differentPortRules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            int maxPort = ushort.MaxValue;
            for (int i = 0; i < numOfRules; ++i)
            {
                Rule r = Utils.GetRandomRule();
                rules.Add(r);
                Rule diffPort = new Rule(r.src, r.dst, r.port >= maxPort ? r.port - 1 : r.port + 1, r.policy);
                differentPortRules.Add(diffPort);
            }

            string fileName = "DifferentPort_1.txt";
            Utils.WriteTable(rules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "DifferentPort_2.txt";
            Utils.WriteTable(differentPortRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.SAT);
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }

        [TestMethod]
        public void OppositeRules()
        {
            const int numOfRules = 100;
            List<Rule> rules = new List<Rule>();
            List<Rule> oppositeRules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            for (int i = 0; i < numOfRules; ++i)
            {
                Rule r = Utils.GetRandomRule();
                rules.Add(r);
                Rule oppositeRule = new Rule(r.src, r.dst, r.port, r.policy == RuleType.Allow ? RuleType.Deny : RuleType.Allow);
                oppositeRules.Add(oppositeRule);
            }

            string fileName = "OppositeRules_1.txt";
            Utils.WriteTable(rules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "OppositeRules_2.txt";
            Utils.WriteTable(oppositeRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.SAT);
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }

        [TestMethod]
        public void SameDstRangeAndExplicit()
        {
            const int numOfRules = 5;

            List<Rule> rangeRules = new List<Rule>();
            List<Rule> explicitRules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            // ranges files
            for (int i = 0; i < numOfRules; ++i)
            {
                Rule r = Utils.GetRandomRuleShortRange();
                rangeRules.Add(r);
                foreach (IPAddress dstIP in IPNetwork.ListIPAddress(r.dst))
                {
                    Console.WriteLine(dstIP);
                    IPAddress netmask = IPAddress.Parse("255.255.255.255");
                    IPNetwork explicitIp = IPNetwork.Parse(dstIP, netmask);
                    explicitRules.Add(new Rule(r.src, explicitIp, r.port, r.policy));
                }
            }

            string fileName = "SameDstRangeAndExplicit_Range.txt";
            Utils.WriteTable(rangeRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "SameDstRangeAndExplicit_Explicit.txt";
            Utils.WriteTable(explicitRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.UNSAT);
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }



        [TestMethod] 
        public void SameSrcRangeAndExplicit()
        {
            const int numOfRules = 5;

            List<Rule> rangeRules = new List<Rule>();
            List<Rule> explicitRules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            // ranges files
            for (int i = 0; i < numOfRules; ++i)
            {
                Rule r = Utils.GetRandomRuleShortRange();
                rangeRules.Add(r);
                foreach (IPAddress srcIP in IPNetwork.ListIPAddress(r.src))
                {
                    Console.WriteLine(srcIP);
                    IPAddress netmask = IPAddress.Parse("255.255.255.255");
                    IPNetwork explicitIp = IPNetwork.Parse(srcIP, netmask);
                    explicitRules.Add(new Rule(explicitIp, r.dst, r.port, r.policy));
                }
            }

            string fileName = "SameSrcRangeAndExplicit_Range.txt";
            Utils.WriteTable(rangeRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            fileName = "SameSrcRangeAndExplicit_Explicit.txt";
            Utils.WriteTable(explicitRules, fileName);
            tables.Add(Z3Firewall.FirewallTable.Load(fileName));
            files.Add(fileName);

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.UNSAT);
            }

            foreach (string f in files)
            {
                System.IO.File.Delete(f);
            }
        }

        [TestMethod]
        public void SameFiles()
        {
            const int numOfRules = 100;
            const int numOfFiles = 4;
            List<Rule> rules = new List<Rule>();
            List<Z3Firewall.FirewallTable> tables = new List<Z3Firewall.FirewallTable>();
            List<string> files = new List<string>();
            for (int i = 0; i < numOfRules; ++i)
            {
                rules.Add(Utils.GetRandomRule());
            }
            for (int i = 0; i < numOfFiles; ++i)
            {
                string fileName = "SameFiles_" + i + ".txt";
                Utils.WriteTable(rules, fileName);
                tables.Add(Z3Firewall.FirewallTable.Load(fileName));
                files.Add(fileName);
            }

            foreach (Z3Firewall.Solver.OrderType order in Enum.GetValues(typeof(Z3Firewall.Solver.OrderType)))
            {
                Z3Firewall.Solver solver = new Z3Firewall.Solver(order);
                foreach (Z3Firewall.FirewallTable table in tables)
                {
                    solver.AddTable(table);
                }
                Z3Firewall.Solver.SolverResult res = solver.solve();
                Assert.AreEqual(res, Z3Firewall.Solver.SolverResult.UNSAT);
            }

            foreach (string fileName in files)
            {
                System.IO.File.Delete(fileName);
            }
        }
    }
}
