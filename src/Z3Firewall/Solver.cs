﻿using Microsoft.Z3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Z3Firewall
{

    public class SatisfiableAssignment
    {
        private Context ctx;
        private Microsoft.Z3.Solver z3solver;

        public SatisfiableAssignment(Context ctx, Microsoft.Z3.Solver z3solver)
        {
            this.ctx = ctx;
            this.z3solver = z3solver;
        }

        public IPAddress SrcAddress()
        {
            var src = ctx.MkConst("src", ctx.IntSort) as IntExpr;
            var srcAddress = Utils.FromLong((z3solver.Model.Eval(src) as IntNum).Int64);
            return srcAddress;
        }

        public IPAddress DstAddress()
        {
            var dst = ctx.MkConst("dst", ctx.IntSort) as IntExpr;
            var dstAddress = Utils.FromLong((z3solver.Model.Eval(dst) as IntNum).Int64);
            return dstAddress;
        }

        public uint PortAddress()
        {
            var port = ctx.MkConst("port", ctx.IntSort) as IntExpr;
            var portAddress = z3solver.Model.Eval(port) as IntNum;
            return portAddress.UInt;
        }
    }

    public class Solver
    {
        private Context ctx;
        private OrderType order;
        private List<BoolExpr> expressions;
        private Microsoft.Z3.Solver z3solver;

        public enum SolverResult
        {
            SAT,
            UNSAT,
            UNKNOWN
        }

        public enum OrderType
        {
            FirstMatch,
            AnyMatch
        }

        public Solver(OrderType order)
        {
            this.ctx = new Context();
            this.order = order;
            this.expressions = new List<BoolExpr>();
        }

        public void AddTable(FirewallTable table)
        {
            var src = ctx.MkConst("src", ctx.IntSort) as IntExpr;
            var dst = ctx.MkConst("dst", ctx.IntSort) as IntExpr;
            var port = ctx.MkConst("port", ctx.IntSort) as IntExpr;

            List<BoolExpr> allowsExpr = new List<BoolExpr>();
            List<BoolExpr> denysExpr = new List<BoolExpr>();

            var p = ctx.MkBool(false);

            foreach (var rule in table.Reverse<FirewallRule>())
            {
                var srcLower = rule.Source.Start.GetUInt();
                var srcUpper = rule.Source.End.GetUInt();

                var dstLower = rule.Destination.Start.GetUInt();
                var dstUpper = rule.Destination.End.GetUInt();

                BoolExpr exp = ctx.MkAnd(ctx.MkGe(src, ctx.MkInt(srcLower)), ctx.MkLe(src, ctx.MkInt(srcUpper)),
                                    ctx.MkGe(dst, ctx.MkInt(dstLower)), ctx.MkLe(dst, ctx.MkInt(dstUpper)),
                                    ctx.MkEq(port, ctx.MkInt(rule.Port))
                                    );
                switch (order)
                {
                    case OrderType.AnyMatch:
                        if (rule.Type == RuleType.Deny)
                        {
                            denysExpr.Add(ctx.MkNot(exp));
                        }
                        else
                        {
                            allowsExpr.Add(exp);
                        }
                        break;

                    case OrderType.FirstMatch:
                        if (rule.Type == RuleType.Deny)
                            p = ctx.MkAnd(ctx.MkNot(exp), p);
                        else
                            p = ctx.MkOr(exp, p);
                        break;
                }
            }

            // build result expression
            switch (order)
            {
                case OrderType.AnyMatch:
                    // add all denys in an NOT AND and all allows in an OR
                    p = ctx.MkAnd(ctx.MkOr(allowsExpr.ToArray()), ctx.MkAnd(denysExpr.ToArray()));
                    break;

                case OrderType.FirstMatch:
                    // p is already prepared in the loop
                    break;
            }
            expressions.Add(p);
        }

        /// <summary>
        /// Iterates over expressions and builds a bool expression where experssion i is equal to i + 1
        /// All expressions are in an AND boolean expression - thus i == i + 1 AND i + 1 == i + 2
        /// </summary>
        /// <returns>Solver status result</returns>
        public SolverResult solve()
        {
            this.z3solver = ctx.MkSolver();
            BoolExpr exp = null;
            for (int i = 0; i < expressions.Count(); ++i)
            {
                for (int j = i + 1; j < expressions.Count(); ++j)
                {
                    if (exp == null)
                    {
                        exp = ctx.MkEq(expressions[i], expressions[j]);
                    }
                    else
                    {
                        exp = ctx.MkAnd(exp, ctx.MkEq(expressions[i], expressions[j]));
                    }
                }
            }
            this.z3solver.Assert(ctx.MkNot(exp));
            return FromZ3Status(z3solver.Check());
        }

        public SatisfiableAssignment GetSatisfiableAssignment()
        {
            return new SatisfiableAssignment(this.ctx, this.z3solver);
        }


        private SolverResult FromZ3Status(Status status)
        {
            if (status == Status.SATISFIABLE)
                return SolverResult.SAT;
            else if (status == Status.UNSATISFIABLE)
                return SolverResult.UNSAT;
            return SolverResult.UNKNOWN;
        }

        ~Solver()
        {
            ctx.Dispose();
        }
    }
}
