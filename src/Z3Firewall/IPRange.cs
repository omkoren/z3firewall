﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Z3Firewall
{
    public class IPRange
    {
        public IPAddress Start { get; set; }
        public IPAddress End { get; set; }

        public static IPRange FromCIDR(string ipCidr)
        {
            string[] parts = ipCidr.Split('.', '/');

            uint ipnum = (Convert.ToUInt32(parts[3]) << 24);

            ipnum |= (Convert.ToUInt32(parts[2]) << 16);
            ipnum |= (Convert.ToUInt32(parts[1]) << 8);
            ipnum |= Convert.ToUInt32(parts[0]);

            int maskbits = Convert.ToInt32(parts[4]);
            uint mask = 0xffffffff;

            var bits = (32 - maskbits);

            if (bits < 32)
                mask <<= bits;
            else
                mask = 0;

            mask = BitConverter.ToUInt32(BitConverter.GetBytes(mask).Reverse().ToArray(), 0);
            
            uint ipstart = ipnum & mask;
            uint ipend = ipnum | (mask ^ 0xffffffff);

            return new IPRange
            {
                Start = new IPAddress(ipstart),
                End = new IPAddress(ipend),
            };
        }

    }
}
