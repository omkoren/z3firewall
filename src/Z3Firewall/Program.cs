﻿using System;

namespace Z3Firewall
{
    class Program
    {
        static void Main(string[] args)
        {
            CApp capp = new CApp(args);
            capp.Run();
            Console.ReadKey();
        }
    }
}
  