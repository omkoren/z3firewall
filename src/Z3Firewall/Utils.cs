﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Z3Firewall
{
    public static class IPAddressExtensions
    {
        public static uint GetUInt(this IPAddress ipAddress)
        {
            var array = ipAddress.GetAddressBytes();


            return BitConverter.ToUInt32(array.Reverse().ToArray(), 0);
        }
    }

    public static class Utils
    {
        public static IPAddress FromLong(long l)
        {
            return new IPAddress(BitConverter.GetBytes(l).Take(4).Reverse().ToArray());
        }
    }
}
