﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z3Firewall
{
    public enum RuleType
    {
        Allow,
        Deny
    }

    public class FirewallRule
    {
        public IPRange Source { get; set; }
        public IPRange Destination { get; set; }
        public int Port { get; set; }
        public RuleType Type { get; set; }
    }

    public class FirewallTable : List<FirewallRule>
    {
        public static FirewallTable Load(string filename)
        {

            var table = new FirewallTable();

            foreach (var line in File.ReadLines(filename))
            {

                var parts = line.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);

                var rule = new FirewallRule();

                rule.Source = IPRange.FromCIDR(parts[0]);
                rule.Destination = IPRange.FromCIDR(parts[1]);
                rule.Port = int.Parse(parts[2]);
                rule.Type = (RuleType)Enum.Parse(typeof(RuleType), parts[3], true);

                table.Add(rule);
            }
            return table;
        }

    }
}
