﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Z3Firewall
{

    ///Type For Parsing Target
    class Options
    {
        public List<string> files;
        [Option('f', "file", Required = true, HelpText = "Firewall file to read.")]
        public string FirewallFile
        {
            get
            {
                if (files != null && files.Count > 0) { return files[0]; }
                return "N/A";
            }
            set
            {
                if (files == null) { files = new List<string>(); }
                files.Add(value);
            }
        }

        public Solver.OrderType orderPolicy;
        [Option('o', "order", Required = true, HelpText =  "Firewall order policy: 'FirstMatch' for first rule order or 'AnyMatch' for any rule order" )]
        public string FirewallOrderPolicy
        {
            get { return orderPolicy.ToString();  }
            set
            {
                if (value == Solver.OrderType.FirstMatch.ToString())
                {
                    orderPolicy = Solver.OrderType.FirstMatch;
                } else if (value == Solver.OrderType.AnyMatch.ToString())
                {
                    orderPolicy = Solver.OrderType.AnyMatch;
                } else
                {
                    orderPolicy = Solver.OrderType.AnyMatch;
                }
            }
        }

        [Option('v', null, HelpText = "Prints info during execution")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
                (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class CApp
    {
        Solver solver;
        Solver.SolverResult result;
        string[] args;
        Options opts;

        public CApp(string[] args)
        {
            this.args = args;
        }

        public void Run()
        {
            try
            {
                Greet();
                if (!GetUserParams())
                {
                    return;
                }
                Compare();
                Results();
            } catch (Exception e)
            {
                if (!opts.Verbose)
                {
                    Console.WriteLine("Exception has occured " + e.Message);
                } else
                {
                    throw e;
                }
            }
        }

        private void Results()
        {
            if (result == Solver.SolverResult.SAT)
            {
                var sat = solver.GetSatisfiableAssignment();
                var srcAddress = sat.SrcAddress();
                if (opts.Verbose) { Console.WriteLine("Result src address " + srcAddress); }
                var dstAddress = sat.DstAddress();
                if (opts.Verbose) { Console.WriteLine("Result dst address " + dstAddress); }
                var portAddress = sat.PortAddress();
                if (opts.Verbose) { Console.WriteLine("Result port address " + portAddress); }

                Console.WriteLine("Firwall not equals: " + srcAddress.ToString() + " ---> " + dstAddress.ToString() + " in port " + portAddress);
            }
            else if (result == Solver.SolverResult.UNSAT)
            {
                Console.WriteLine("Firewall Equals!!!");
            }
            else
            {
                Console.WriteLine("Cannot decide - UNKNOWN!!!");
            }

        }

        private void Compare()
        {
            List<FirewallTable> tables = new List<FirewallTable>();
            foreach (var f in opts.files)
            {
                if (opts.Verbose) { Console.WriteLine("Loading firewall table for file " + f); }
                tables.Add(FirewallTable.Load(f));
            }
            if (opts.Verbose) { Console.WriteLine("Creating solver: order " + opts.orderPolicy.ToString()); }
            solver = new Solver(opts.orderPolicy);

            if (opts.Verbose) { Console.WriteLine("Adding firewall tabels"); }
            foreach (var t in tables)
            {
                solver.AddTable(t);
            }
            if (opts.Verbose) { Console.WriteLine("Solving..."); }
            result = solver.solve();
            if (opts.Verbose) { Console.WriteLine("Solver returned " + result.ToString()); }
        }

        private void Greet()
        {
            System.Console.WriteLine("Welcome! This program will compare firewall files for semantic difference.");
            System.Console.WriteLine("------------------------------------------------------------------------------");
        }

        private bool GetUserParams()
        {
            Options options = new Options();
            CommandLine.Parser parser = new Parser();
            if (parser.ParseArguments(args, options))
            {
                // verbose
                if (options.Verbose)
                {
                    foreach(var f in options.files)
                        Console.WriteLine("Arguemnt File: " + f);
                    Console.WriteLine("Argument Order Policy: " + options.FirewallOrderPolicy);
                }
                // validate args
                if (options.files.Count < 2)
                {
                    Console.WriteLine("Please provide at least two files to compare");
                    return false;
                }
                this.opts = options;
                return true;
            } else
            {
                Console.WriteLine(options.GetUsage());
            }
            return false;
        }
    }
}

