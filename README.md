# Firewall Packet Filter Tables Comparison

## Documentation

A full documentation of the of the project can be downloaded from [Here](./docs/Documentation.docx).

## Build Instructions

* Install the Microsoft .NET Framework 4.6.2 Developer Pack   
from: https://www.microsoft.com/en-us/download/details.aspx?id=53321
* Run ``./build.bat``
* You will find your artifacts under ./src/Z3Firewall/bin/Debug

## Run Instructions
Z3Firewall.exe -f <full path to firewall rule file> -f <full path firewall rule file> -o <order: FirstMatch or AnyMatch> [-v]
Supply at least two full path to files or put them in the executable directory for local path.
Supply an order for reading the firewall files - FirstMatch for first rule match or AnyMatch if order is not important
Firewall rule files should be a simple text files where each row is a rule. Each column in the rule is separated by TAB:
-----------------------------------------------------
192.168.1.0/24	111.111.123.5/22	8000	allow
-----------------------------------------------------
SRC IP/NETMASK	DST IP/NETMASK		PORT	RULE TYPE
For examples see example files Firewall1.txt, Firewall2.txt or Firewall3.txt

## Output
The program will print if the firewall files supplied respecting the order have a semantic difference or not.
If the files are different an example of a rule will be printed to the screen to support the claim.
* Semantic difference:
cmd>Z3Firewall.exe -f Firewall1.txt -f Firewall2.txt -o FirstMatch
Welcome! This program will compare firewall files for semantic difference.
------------------------------------------------------------------------------
Firwall not equals: 192.168.1.6 ---> 0.0.0.0 in port 80
* Files are the same:
cmd>Z3Firewall.exe -f Firewall1.txt -f Firewall1.txt -o FirstMatch
Welcome! This program will compare firewall files for semantic difference.
------------------------------------------------------------------------------
Firewall Equals!!!



